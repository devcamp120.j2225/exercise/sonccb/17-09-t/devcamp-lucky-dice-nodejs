// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import mongooseJS
const mongoose = require("mongoose");

const path = require("path");

// Khởi tạo app express
const app = express();

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
 extended: true
}));

// Khai báo cổng của project
const port = 8000;

//Import router
const userRouter = require("./app/routes/userRouter");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");
const diceRouter = require("./app/routes/diceRouter");



//Import model
// const prizeModal = require("./app/models/prizeModel");
// const voucherModal = require("./app/models/voucherModel");
// const prizeHistoryRouter = require("./app/models/prizeHistoryModel");
// const voucherHistoryRouter = require("./app/models/voucherHistoryModel");

//Import middleware


// mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (err) => {
//     if(err) {
//         throw err;
//     }

//     console.log("Connect MongoDB successfully!");
// });

app.use((request, response, next) => {
 console.log("Time", new Date());
 next();
},
(request, response, next) => {
 console.log("Request method: ", request.method);
 next();
}
)

// Khai báo API
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/app/views/luckyDice.html"));
})
app.use(express.static(__dirname + "/app/views"));



mongoose.connect("mongodb://localhost:27017/CRUD_LuckyDice365", function(error) {
    if (error) throw error;

    console.log('Successfully connected!');
   })

 app.use(userRouter);
 app.use(diceHistoryRouter);
 app.use(prizeRouter);
 app.use(voucherRouter);
 app.use(prizeHistoryRouter);
 app.use(voucherHistoryRouter);
 app.use(diceRouter);



// Chạy app express
app.listen(port, () => {
 console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
});
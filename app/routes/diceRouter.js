const { Router } = require("express");
const express = require("express");

const router = express.Router();

const {
 diceHandler,
 getDiceHistoryByUsername
} = require("../controllers/diceController");

router.post("/devcamp-lucky-dice/dice", diceHandler);


module.exports = router;
const express = require("express");

const router = express.Router();

const {
 createPrizeHistory,
  getAllPrizeHistory,
  getPrizeHistoryById,
  getAllPrizeHistoryOfUser,
  updatePrizeHistoryById,
  deletePrizeHistoryById
} = require("../controllers/prizeHistoryController");

router.post("/prize-histories", createPrizeHistory);

router.get("/prize-histories", getAllPrizeHistory);

router.get("/prize-histories/:prizeHistoryId", getPrizeHistoryById);

router.get("/devcamp-lucky-dice/prize-history", getAllPrizeHistoryOfUser);

router.put("/prize-histories/:prizeHistoryId", updatePrizeHistoryById);

router.delete("/prize-histories/:prizeHistoryId",deletePrizeHistoryById);

module.exports = router;

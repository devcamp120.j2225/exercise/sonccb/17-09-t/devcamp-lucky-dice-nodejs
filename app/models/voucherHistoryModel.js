//import thư viện mongoose
const mongoose = require("mongoose");


//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo course Schema
const voucherHistorySchema = new Schema({
 user:[
  {
   type:mongoose.Types.ObjectId,
   ref:"User",
   required:true
  }
 ],
 voucher:[
  {
   type:mongoose.Types.ObjectId,
   ref:"Voucher",
   required:true
  }
 ]
},{
 timestamps:true
}
)
module.exports = mongoose.model("VoucherHistory",voucherHistorySchema);
"use strict";
        /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const gREQUEST_STATUS_OK = 200;
        const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

        const gBASE_URL = "/devcamp-lucky-dice";

        var gTable = ""
        /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

        /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        //Hàm xữ lý nút ném
        var onBtnNemClick = () => {
            "use strict";
            //Dùng hàm getDataFormInput truyền dữ liệu input vào UserDataObject
            var vUserDataObject = getDataFormInput();
            // Hàm kiểm tra thông tin input xem có hợp lệ ko nếu hợp lệ thì trả  vCheck về true còn không thì cảnh báo và trả vCheck về false
            var vCheck = validateDaTaObject(vUserDataObject);
            console.log(vCheck);
            //Nếu vCheck = true thì consolelog ra "Dữ liệu hợp lệ" nếu false thì consolelog ra "Dữ liệu không hợp lệ"
            if (vCheck == true) {
                console.log("Dữ liệu hợp lệ");
                //Gọi server lấy responseText ghi ra console.log
                callAjaxCreateDataDice(vUserDataObject);
            } else{
                console.log("Dữ liệu không hợp lệ");
            }
        }
        //Hàm nút nhấn Dice History
        var onBtnDiceHistoryClick = () => {
            "use strict"
            console.log("Nút Dice History được ấn");
             //Dùng hàm getDataFormInput truyền dữ liệu input vào UserDataObject
             var vUserDataObject = getDataFormInput();
            // Hàm kiểm tra thông tin input xem có hợp lệ ko nếu hợp lệ thì trả  vCheckvề true còn không thì cảnh báo và trả vCheck về false
            var vCheck = validateDaTaObject(vUserDataObject);
            if (vCheck == true) {
                console.log("Dữ liệu hợp lệ");
                //Gọi server lấy responseText ghi ra console.log
                callAPIDataHistoryDice(vUserDataObject);
            } else{
                console.log("Dữ liệu không hợp lệ");
            }
        }
        /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        //Hàm xóa hết phần tử row trong mảng 
        var refreshDiceHistoryTable = () => {
            "use strict"
            var vTable = document.getElementById("history-dice-table");
            while (vTable.rows.length > 1) {
                vTable.deleteRow(1);
            }
        }
        
        //Hàm ghi già trị dice history ra table
         var writeObjectDiceHistoryToTable = (vDiceHistoryObject) => {
            "use strict";
            const vEND_OF_ROW = -1;
            const vCOLUMM_OF_TURN = 0;
            const vCOLUMM_OF_DICE = 1;
            var vTableDiceHistoryElement = document.getElementById("history-dice-table");
            var vTable = vTableDiceHistoryElement.getElementsByTagName("tbody")[0];
            var vDiceHistory = vDiceHistoryObject.dices;
            for (var bIndex = 0 ; bIndex < vDiceHistory.length; bIndex ++) {
                var vRow = vTable.insertRow(vEND_OF_ROW);
                var vCellTurn = vRow.insertCell(vCOLUMM_OF_TURN);
                var vCellDice = vRow.insertCell(vCOLUMM_OF_DICE);
                vCellTurn.innerHTML = bIndex + 1;
                vCellDice.innerHTML = vDiceHistory[bIndex];
            }
         }

        //Hàm onReadyStateChange của dice history
        var onReadyStateChangeDiceHIstory = (paramXMLHttpDiceHistory) => {
            "use strict";
            var vXmlHttpDiceHistory = paramXMLHttpDiceHistory;
            vXmlHttpDiceHistory.onreadystatechange =  () => {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                    var vResponse = vXmlHttpDiceHistory.responseText;
                    //Ghi ra consolelog object vResponse dưới dạng string
                    console.log(vResponse);
                    //Chuyển vResponse dưới dang string thành object và gán vào vDiceHistoryObject
                    var vDiceHistoryObject = JSON.parse(vResponse);
                    //Ghi ra console.log vDiceHistoryObject
                    console.log(vDiceHistoryObject);
                    //Xóa hết phần tử row trong mảng
                    refreshDiceHistoryTable();
                    //Ghi già trị dice history ra table
                    writeObjectDiceHistoryToTable(vDiceHistoryObject);
                }
            }
        }
        //Hàm api ibject dạng text của dice history
        var callAPIDataHistoryDice = (paramDataObject) => {
            "use strict";
            var vUserNameTest = paramDataObject.username;

            var vXmlHttpDiceHistory = new XMLHttpRequest();
            vXmlHttpDiceHistory.open("GET", gBASE_URL + "/dice-history?username=" + vUserNameTest, true);
            vXmlHttpDiceHistory.send();
            onReadyStateChangeDiceHIstory(vXmlHttpDiceHistory);
        }
        //Hiển thị prizes nếu có
        var changePrizesPresent = (paramPrize) => {
            "use strict";
            var vImgPrizeElement = document.getElementById("img-prize");
            var vPrize = paramPrize;
            switch(vPrize) {
                case "Mũ":
                    vImgPrizeElement.src = "LuckyDiceImages/hat.jpg";
                    break;
                case "Áo":
                    vImgPrizeElement.src = "LuckyDiceImages/t-shirt.png";
                    break;
                case "Ô tô":
                    vImgPrizeElement.src = "LuckyDiceImages/car.jpg";
                    break; 
                case "Xe máy":
                    vImgPrizeElement.src = "LuckyDiceImages/xe_may.png";
                    break;
                default:
                    vImgPrizeElement.src = "LuckyDiceImages/no-present.jpg";
            }
        }
        
        //Hàm ghi ra voucher
        var changeVoucherID = (paramVoucher, paramDIceObject) => {
            "use strict";
            var vPVoucherIdElement = document.getElementById("p-id");
            var vPVoucherDiscountElement = document.getElementById("p-discount");
            var vVoucher = paramVoucher;
            if (vVoucher == null) {
                vPVoucherIdElement.innerHTML = "ID:0000"
                vPVoucherDiscountElement.innerHTML = "0%"
            } else {
                var vVoucherId = paramDIceObject.voucher.maVoucher;
                var vVoucherDiscount = paramDIceObject.voucher.phanTramGiamGia;
                vPVoucherIdElement.innerHTML = "ID:" +vVoucherId
                vPVoucherDiscountElement.innerHTML = vVoucherDiscount + "%"
            }
        }

        //Hàm ghi ra thông báo
        var changeNotification =(paramDIce) => {
            "use strict";
            var vPNotification = document.getElementById("p-thongbao");
            if (paramDIce < 4) {
                vPNotification.innerHTML = "Chúc bạn may mắn lần sau !!!";
            } else {
                vPNotification.innerHTML = "Xin chúc bạn hãy chơi tiếp !!!";
            }
        }
        
        //Hàm chuyển đổi ảnh dựa vào số xúc xắc
        var changeImgDiceWithResult = (paramDiceObject) => {
            "use strict";
            var vImgDiceElement = document.getElementById("img-dice");
            var vDice = paramDiceObject;
            if (vDice == 1) {
                vImgDiceElement.src = "LuckyDiceImages/1.png";
            } else  if (vDice == 2) {
                vImgDiceElement.src = "LuckyDiceImages/2.png";
            } else  if (vDice == 3) {
                vImgDiceElement.src = "LuckyDiceImages/3.png";
            } else  if (vDice == 4) {
                vImgDiceElement.src = "LuckyDiceImages/4.png";
            } else  if (vDice == 5) {
                vImgDiceElement.src = "LuckyDiceImages/5.png";
            } else  if (vDice == 6) {
                vImgDiceElement.src = "LuckyDiceImages/6.png";
            }
        }
        //Hàm chuyển giá trị Object dạng text sang Object
        var changeResponseTextToOBjec = (paramXMLHttp) => {
            "use strict";
            var  vDiceObject = JSON.parse(paramXMLHttp.responseText);
            return vDiceObject;
        }

        // Hàm kiểm tra thông sô status
        var onReadyStateChangeDice = (paramXMLHttp) => {
            "use strict";
            var vXMLHttpDice = paramXMLHttp;
            vXMLHttpDice.onreadystatechange =  () => {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {  //nếu trạng thái của response ready và ok
                writeResponseTextToConsoleLog(vXMLHttpDice);
                //Hàm chuyển giá trị Object dạng text sang Object
                var vDiceObject = changeResponseTextToOBject(vXMLHttpDice);
                 //Ghi ra object vDiceObject
                console.log(vDiceObject);
                //chuyển đổi ảnh dựa vào số xúc xắc
                var vDice = vDiceObject.dice;
                changeImgDiceWithResult(vDice);
                //ghi ra thông báo
                changeNotification(vDice);
                //Hiển thị ra voucher(nếu có)
                var vVoucher = vDiceObject.voucher;
                changeVoucherID(vVoucher , vDiceObject);
                //Hiển thị prizes nếu có
                var vPrize = vDiceObject.prize;
                changePrizesPresent(vPrize);
                }
            }
        }
        // Hàm ghi data ra consolelog
        var writeResponseTextToConsoleLog = (paramXMLHttp) => {
            "use strict";
            console.log(paramXMLHttp.responseText);
        }

        //Gọi server lấy responseText
        var callAjaxCreateDataDice = (paramDataObject) => {
            "use strict";
          $.ajax({
           url:gBASE_URL + "/dice",
           type:"POST",
           con

          })
        }
        //Hàm kiểm tra dữ liệu
        var validateDaTaObject = (paramDataObject) => {
            "use strict";
            var vResult = true;
            var vUsername = paramDataObject.username.trim();
            var vFirstname = paramDataObject.firstname.trim();
            var vLastname = paramDataObject.lastname.trim();
            if (vUsername == "") {
                alert("Chưa nhập username");
                vResult = false;
            } else if (vFirstname == "") {
                alert("Chưa nhập firstname");
                vResult = false;
            } else if ( vLastname == "") {
                alert("Chưa nhập lastname");
                vResult = false;
            }
            return vResult;
        }

        //Hàm lấy thông tin input chuyền vào hướng đối tượng vUserDataObject
        var getDataFormInput = () => {
            "use strict";
            var vUserDataObject = {
                username: "",
                firstname: "",
                lastname: ""
            }
            vUserDataObject.username = $("input-username").val();
            vUserDataObject.firstname = $("input-firstname").val();
            vUserDataObject.lastname = $("input-lastname").val();

            return vUserDataObject;
        }
// Import thư viện mongoose
const mongoose = require("mongoose");

// Import User Model
const prizeHistoryModel = require("../models/prizeHistoryModel");
const userModel = require("../models/userModel");

const createPrizeHistory = (req ,res) =>{
 //B1: Chuẩn bị dữ liệu
 let body = req.body;

 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(body.userId)) {
  return res.status(400).json({
      status: "Error 400: Bad req",
      message: "User ID is invalid"
  })
}
 if(!mongoose.Types.ObjectId.isValid(body.prizeId)) {
  return res.status(400).json({
      status: "Error 400: Bad req",
      message: "Prize ID is invalid"
  })
}
 //B3: Thao tác với cơ sở dữ liệu
 let newPrizeHistory = {
  _id: mongoose.Types.ObjectId(),
  user: body.userId,
  prize: body.prizeId
}
prizeHistoryModel.create(newPrizeHistory, (error, data) => {
  if(error) {
      return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
      })
  } else {
      return res.status(201).json({
         status: "Create data success",
         data: data
      })
  }
})
}

const getAllPrizeHistory = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let user = req.query.user;
 console.log("UserId:",user)
  //tạo ra điều kiện lọc
  let condition = {};
  //B2: Validate dữ liệu
  if (user) {
     //condition.title = courseName;        
     condition.user =  user;
  }
 prizeHistoryModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get prize history success",
                data: data
            })
        }
    })
}

const getPrizeHistoryById  = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let prizeHistoryId = req.params.prizeHistoryId;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Prize History ID is not valid"
     })
 }
 //B3: Thao tác với cơ sở dữ liệu
 prizeHistoryModel.findById(prizeHistoryId, (error, data) => {
     if(error) {
         return res.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return res.status(200).json({
             status: "Success: Get prize history success",
             data: data
         })
     }
 })
}

const getAllPrizeHistoryOfUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let username = req.query.username;
    let condition = {};
    // B2: Validate dữ liệu
    if (!username) {
        return res.status(400).json({
            message: 'Username is required!'
        })
    } else {
        userModel.findOne({ username: username }, (error, userExist) => {
            if (error) {
                return res.status(500).json({
                        message: error.message
                })
            } else {
                if (!userExist) {
                        return res.status(200).json({
                            message: 'Username does not exist!',
                            prizeHistories: []
                    })
                } else {
                        condition.user = userExist._id
                        prizeHistoryModel.find(condition)
                            .populate("prize")
                            .exec((error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                            message: error.message
                                    })
                                }
                                return res.status(200).json({
                                    message: `Get all prize histories of username ${username} successfully`,
                                    prizeHistory: data
                                })
                            })
                }
            }
        })
    }
}

const updatePrizeHistoryById  = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let prizeHistoryId = req.params.prizeHistoryId;
 let body = req.body;

 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Prize History ID is not valid"
     })
 }
 if(!mongoose.Types.ObjectId.isValid(body.userId) && body.userId) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "UserId ID is not valid"
     })
 }
 if(!mongoose.Types.ObjectId.isValid(body.prizeId)  && body.prizeId) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Prize ID is not valid"
     })
 }
 
 //B3: Thao tác với cơ sở dữ liệu
 let prizeHistoryUpdate = {
   user: body.userId,
   prize: body.prizeId
 }

 prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, prizeHistoryUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return res.status(200).json({
             status: "Success: Update Prize History success",
             data: data
         })
     }
 })
}

const deletePrizeHistoryById = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let prizeHistoryId = req.params.prizeHistoryId;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "prize History ID is not valid"
     })
 }

 //B3: Thao tác với cơ sở dữ liệu
 prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (error) => {
     if(error) {
         return res.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
        return res.status(204).json({
            status: `Delete Prize History ID ${prizeHistoryId} successfully!`
        })
     }
 })
}


module.exports = {
 createPrizeHistory,
 getAllPrizeHistory,
 getAllPrizeHistoryOfUser,
 getPrizeHistoryById,
 updatePrizeHistoryById,
 deletePrizeHistoryById
}
// Import thư viện mongoose
const mongoose = require("mongoose");

// Import User Model
const voucherHistoryModel = require("../models/voucherHistoryModel");
const userModel = require("../models/userModel");

const createVoucherHistory = (req ,res) =>{
 //B1: Chuẩn bị dữ liệu
 let body = req.body;

 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(body.userId)) {
  return res.status(400).json({
      status: "Error 400: Bad req",
      message: "User ID is invalid"
  })
}
 if(!mongoose.Types.ObjectId.isValid(body.voucherId)) {
  return res.status(400).json({
      status: "Error 400: Bad req",
      message: "Voucher ID is invalid"
  })
}
 //B3: Thao tác với cơ sở dữ liệu
 let newVoucherHistory = {
  _id: mongoose.Types.ObjectId(),
  user: body.userId,
  voucher: body.voucherId
}
voucherHistoryModel.create(newVoucherHistory, (error, data) => {
  if(error) {
      return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
      })
  } else {
      return res.status(201).json({
         status: "Create data success",
         data: data
      })
  }
})
}

const getAllVoucherHistory = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let user = req.query.user;
 console.log("UserId:",user)
  //tạo ra điều kiện lọc
  let condition = {};
  //B2: Validate dữ liệu
  if (user) {
     //condition.title = courseName;        
     condition.user =  user;
  }
 voucherHistoryModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get voucher history success",
                data: data
            })
        }
    })
}

const getVoucherHistoryById  = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let voucherHistoryId = req.params.voucherHistoryId;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Voucher History ID is not valid"
     })
 }
 //B3: Thao tác với cơ sở dữ liệu
 voucherHistoryModel.findById(voucherHistoryId, (error, data) => {
     if(error) {
         return res.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return res.status(200).json({
             status: "Success: Get Voucher history success",
             data: data
         })
     }
 })
}

const getAllVoucherHistoryOfUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let username = req.query.username;
    let condition = {};
    // B2: Validate dữ liệu
    if (!username) {
        return res.status(400).json({
            message: 'Username is required!'
        })
    } else {
        userModel.findOne({ username: username }, (error, userExist) => {
            if (error) {
                return res.status(500).json({
                        message: error.message
                })
            } else {
                if (!userExist) {
                        return res.status(200).json({
                            message: 'Username does not exist!',
                            diceHistories: []
                    })
                } else {
                        condition.user = userExist._id
                        voucherHistoryModel.find(condition)
                            .populate("voucher")
                            .exec((error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                            message: error.message
                                    })
                                }
                                return res.status(200).json({
                                    message: `Get all voucher histories of username ${username} successfully`,
                                    voucherHistory: data
                                })
                            })
                }
            }
        })
    }
}

const updateVoucherHistoryById  = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let voucherHistoryId = req.params.voucherHistoryId;
 let body = req.body;

 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Voucher History ID is not valid"
     })
 }
 if(!mongoose.Types.ObjectId.isValid(body.userId) && body.userId) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "UserId ID is not valid"
     })
 }
 if(!mongoose.Types.ObjectId.isValid(body.voucherId)  && body.voucherId) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Voucher ID is not valid"
     })
 }
 
 //B3: Thao tác với cơ sở dữ liệu
 let VoucherHistoryUpdate = {
   user: body.userId,
   voucher: body.voucherId
 }

 voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, VoucherHistoryUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return res.status(200).json({
             status: "Success: Update Voucher History success",
             data: data
         })
     }
 })
}

const deleteVoucherHistoryById = (req, res) => {
 //B1: Chuẩn bị dữ liệu
 let voucherHistoryId = req.params.voucherHistoryId;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
     return res.status(400).json({
         status: "Error 400: Bad req",
         message: "Voucher History ID is not valid"
     })
 }

 //B3: Thao tác với cơ sở dữ liệu
 voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (error) => {
     if(error) {
         return res.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
        return res.status(204).json({
            status: `Delete Voucher History ID ${voucherHistoryId} successfully!`
        })
     }
 })
}


module.exports = {
 createVoucherHistory,
 getAllVoucherHistory,
 getVoucherHistoryById,
 getAllVoucherHistoryOfUser,
 updateVoucherHistoryById,
 deleteVoucherHistoryById
}